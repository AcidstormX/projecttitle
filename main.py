#! /usr/bin/python

import random
import pygame
import math 
from plasma_ball import PlasmaBall
from enemies import enemies

display_width = 640
display_height = 480
clock = pygame.time.Clock()
fps = 60

pygame.mixer.pre_init(44100, -16, 2, 512)
pygame.mixer.init()
background_music = pygame.mixer.Sound("sound/background_music.wav")

def mul((x, y), s):
    return (x*s, y*s)

def magn((x, y)):
    return math.sqrt(x*x + y*y)

def norm((x, y)):
    vmag = magn((x, y))
    return (x/vmag, y/vmag)

keys = {pygame.K_UP:    ( 0, -1),
        pygame.K_DOWN:  ( 0,  1),
        pygame.K_LEFT:  (-1,  0),
        pygame.K_RIGHT: ( 1,  0),}

#BACKGROUND

class Background(pygame.sprite.Sprite):
    def __init__(self, state, parent=None, speed=2, rot=0):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.rotate(pygame.image.load("data/background1.png").convert(), rot)
        self.image.set_alpha(100)
        self.rect = self.image.get_rect()
        state.other.add(self)
        self.speed = speed
        
        self.child = Background(state, self, speed, rot) if not parent else None

    def update(self):
        self.rect.move_ip(0, self.speed)
        if self.child and self.child.rect.top == 0:
            self.rect.top = 0
            self.child.rect.top = -self.rect.height

#SHIP

class Ship(pygame.sprite.Sprite):
    def __init__(self, state, config):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(config.get('Image', 'data/player.png')).convert_alpha()
        self.health = config.get('Health', 100)
        self.speed = config.get('Speed', 5)
        self.laser_sound = pygame.mixer.Sound("sound/player_laser.wav")
        self.rect = self.image.get_rect(top=200, left=200)
        self.mask = pygame.mask.from_surface(self.image)
        self.state = state

        if 'AI' in config:
            state.sprites.add(self)
            self.ai = config['AI']
        else:
            state.player = self
            state.playersprite.add(self)

    def update(self):
        self.ai(self)

    def suffer(self, damage):
        blob_death = pygame.mixer.Sound("sound/blob_die.wav")
        player_death = pygame.mixer.Sound("sound/player_die.wav")

        self.health -= damage
        if self.health <= 0:
            blob_death.play()
            self.kill()

#PLAYER

class Player(Ship):
    def __init__(self, state):
        Ship.__init__(self, state, {})
        
    def update(self):
        pressed = pygame.key.get_pressed()
        vs = [v for (k, v) in keys.iteritems() if pressed[k]]
        if vs:
            v = map(sum, zip(*vs))
            if magn(v):
                v = norm(v)
                self.rect.move_ip(mul(v, self.speed))
    
    def shoot(self):
        self.laser_sound.play()
        PlasmaBall(self.state, self.rect.move((0, 10)).topleft)
        PlasmaBall(self.state, self.rect.move((-10, 10)).topright)

    def keydown(self, key):
        action = actions.get(key, None)
        if action: action(self)

actions = {pygame.K_SPACE: Player.shoot}

#STATE

class State(object):
    def __init__(self, screen):
        self.screen = screen
        self.screen_rect = screen.get_rect()
        self.sprites = pygame.sprite.Group()
        self.player = None
        self.playersprite = pygame.sprite.GroupSingle()
        self.bullets = pygame.sprite.Group()
        self.other = pygame.sprite.Group()
        self.allgroups = [self.other, self.bullets, self.sprites, self.playersprite]
        
    def draw(self, screen):
        for g in self.allgroups:
            g.draw(screen)
    def update(self):
        for g in self.allgroups:
            g.update()


# MAIN

def main():
    pygame.init()

    screen = pygame.display.set_mode((display_width, display_height))
    screen_rect = screen.get_rect()
    pygame.display.set_caption('ProjectTitle')
    bg = pygame.image.load("data/background1.png").convert()

    pygame.mouse.set_visible(False)
    
    state = State(screen)
    Background(state, speed=1, rot=0)
    Background(state, speed=2, rot=180)
    Player(state).rect.center = (320, 420)
    
    Ship(state, enemies['Blob']).rect.center = (100, 100)
    Ship(state, enemies['Blob']).rect.center = (540, 100)
    Ship(state, enemies['Mother Blob']).rect.center = (320, 200)
    
    background_music.set_volume(.04)
    state.player.laser_sound.set_volume(0.8)
    
    background_music.play(-1)

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                state.player.keydown(event.key)
        screen.fill((0, 0, 0))

        en_encounter = random.randint(0,170)
        if en_encounter == 1:
            Ship(state, enemies['Blob']).rect.center = (random.randint(100, 540), random.randint(100, 220))

        mother_encounter = random.randint(0,800)
        if mother_encounter == 1:
            Ship(state, enemies['Mother Blob']).rect.center = (320, 200)

        state.draw(screen)
        healthbar = pygame.draw.rect(screen, (255, 0, 0),[5,5,100,8])
        if state.player.health > 0:
            health = pygame.draw.rect(screen, (0, 225, 0), [5,5,state.player.health,8])

        state.update()
        state.player.rect.clamp_ip(screen_rect)
        pygame.display.update()
        clock.tick(fps)

if __name__ == '__main__':
    main()
