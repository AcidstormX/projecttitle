import random
import pygame
from plasma_ball import PlasmaBall

def blob_ai(shootrate, plasma_ball_speed, sound):
    def func(this):
        laser = pygame.mixer.Sound(sound)
        if random.randint(0, shootrate) == 0:
            laser.play()
            p = PlasmaBall(this.state, this.rect.move((27, 0)).bottomleft)
            p.speed = -plasma_ball_speed
    return func

def mother_blob_ai(shootrate, plasma_ball_speed):
    def func(this):
        if random.randint(0, shootrate) == 0:
            right_canon = PlasmaBall(this.state, this.rect.move((30, -8)).bottomleft)
            left = PlasmaBall(this.state, this.rect.move((90, -8)).bottomleft)
            right_canon.speed = -plasma_ball_speed
            left.speed = -plasma_ball_speed
    return func

enemies = {'Blob': {'Health': 80,
                    'Image': 'data/enemySpaceship.png',
                    'AI': blob_ai(100, 3, "sound/blob_laser.wav")},
           'Mother Blob': {'Health': 200,
                        'Image': 'data/enemyShip.gif',
                        'AI': mother_blob_ai(55, 5)}}