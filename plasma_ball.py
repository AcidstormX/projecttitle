import pygame

COLORKEY = (85,72,123)
class PlasmaBall(pygame.sprite.Sprite):
    def __init__(self, state, (x, y)):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.surface.Surface((10, 10))
        self.image.set_colorkey(COLORKEY)
        self.image.fill(COLORKEY)
        pygame.draw.circle(self.image, (125, 225, 200), (5, 5), 4)
        pygame.draw.circle(self.image, (255, 120, 0),   (5, 5), 3)
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = self.image.get_rect(top=y, left=x)
        self.speed = 8
        self.state = state
        state.bullets.add(self)
        self.damage = 20

    def update(self):
        self.rect.move_ip((0, -self.speed))
        
        if not self.state.screen_rect.contains(self.rect):
            self.kill()
            return

        targets = pygame.sprite.spritecollide(self, self.state.sprites, False, pygame.sprite.collide_mask) + \
                  pygame.sprite.spritecollide(self, self.state.playersprite, False, pygame.sprite.collide_mask)

        if targets:
            self.kill()
        for target in targets:
            target.suffer(self.damage)